;;;;;;;;;;;;;;;;;; this has to come before anything from package.el
(setq debug-on-error t)
(setq emacs-dir "~/.emacs.d"
      modules-dir (concat emacs-dir "/modules"))

(require 'package)
;(add-to-list 'package-archives '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/"))
;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(setq custom-file (concat emacs-dir "/customs.el"))
(load custom-file)

(require 'use-package)
(setq use-package-always-ensure t)
;;;;;;;;;;;;;;;;;; packages
(use-package visual-regexp
  :bind (("C-c r" . vr/replace)
         ("C-c q" . vr/query-replace)))
(use-package ace-jump-mode
  :bind ("M-SPC" . ace-jump-mode))
(use-package iedit
  :bind ("C-;" . iedit-mode))
(use-package recentf
  :bind ("C-x C-r" . ido-recentf-open))
(use-package auto-complete)
(use-package yasnippet
  :init (yas-global-mode 1))
(use-package web-mode
  :init (setq auto-mode-alist (cons '("\\.html\\.erb$" . web-mode) auto-mode-alist)))
(use-package tex
  :ensure auctex
  :config (add-hook 'LaTeX-mode-hook 'visual-line-mode))
(use-package lush-theme)
(use-package indent-guide
  :init (indent-guide-global-mode))


;;;;;;;;;;;;;;;;;; General Key Mappings
(global-set-key (kbd "C-s")        'isearch-forward-regexp)
(global-set-key (kbd "C-r")        'isearch-backward-regexp)
(global-set-key (kbd "C-M-s")      'isearch-forward)
(global-set-key (kbd "C-M-r")      'isearch-backward)

(define-key global-map (kbd "M-a") 'align-regexp)
(global-set-key (kbd "M-z")        'zap-up-to-char)
(global-set-key (kbd "C-`")        'push-mark-no-activate)
(global-set-key (kbd "M-`")        'jump-to-mark)
(global-set-key (kbd "C-M-t")      'touchpad-toggle)
;;;;;;;;;;;;;;;;;;

(load (concat modules-dir "/general-settings"))
(load (concat modules-dir "/haskell"))
(load (concat modules-dir "/org-stuff"))
(load (concat modules-dir "/helper-funcs"))

;;;;;;;;;;;;;; Start the Server
; (setq server-name "main-server") ; default is "server"
(server-start)
