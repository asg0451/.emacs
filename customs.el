;;;;;;;;;;;; Customize-Generated Config
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-ignore-case nil)
 '(ac-modes
   (quote
    (emacs-lisp-mode lisp-mode lisp-interaction-mode slime-repl-mode c-mode cc-mode c++-mode go-mode java-mode malabar-mode clojure-mode clojurescript-mode scala-mode scheme-mode ocaml-mode tuareg-mode coq-mode haskell-mode agda-mode agda2-mode perl-mode cperl-mode python-mode ruby-mode lua-mode ecmascript-mode javascript-mode js-mode js2-mode php-mode css-mode makefile-mode sh-mode fortran-mode f90-mode ada-mode xml-mode sgml-mode ts-mode sclang-mode verilog-mode web-mode)))
 '(ac-use-fuzzy t)
 '(align-exclude-rules-list
   (quote
    ((exc-dq-string
      (regexp . "\"\\([^\"
]+\\)\"")
      (repeat . t)
      (modes . align-dq-string-modes))
     (exc-sq-string
      (regexp . "'\\([^'
]+\\)'")
      (repeat . t)
      (modes . align-sq-string-modes))
     (exc-open-comment
      (regexp .
              #[514 "\211\203 \301\202	 \302\303\304!\305Q\306#\207"
                    [comment-start re-search-backward re-search-forward "[^
\\\\]" regexp-quote "\\(.+\\)$" t]
                    6 "

(fn END REVERSE)"])
      (modes . align-open-comment-modes))
     (exc-c-comment
      (regexp . "/\\*\\(.+\\)\\*/")
      (repeat . t)
      (modes . align-c++-modes))
     (exc-c-func-params
      (regexp . "(\\([^)
]+\\))")
      (repeat . t)
      (modes . align-c++-modes))
     (exc-c-macro
      (regexp . "^\\s-*#\\s-*\\(if\\w*\\|endif\\)\\(.*\\)$")
      (group . 2)
      (modes . align-c++-modes))
     (Xresources\ comments
      (regexp . "\\(.*!.*\\)")))))
 '(align-open-comment-modes
(quote
 (vhdl-mode emacs-lisp-mode lisp-interaction-mode lisp-mode scheme-mode c++-mode c-mode java-mode perl-mode cperl-mode python-mode makefile-mode conf-xdefaults-mode)))
'(ansi-color-faces-vector
[default default default italic underline success warning error])
'(ansi-color-names-vector
["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#657b83"])
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes (quote (lush)))
'(custom-safe-themes
(quote
 ("0820d191ae80dcadc1802b3499f84c07a09803f2cb90b343678bdb03d225b26b" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "1ba463f6ac329a56b38ae6ac8ca67c8684c060e9a6ba05584c90c4bffc8046c3" default)))
 '(display-battery-mode t)
 '(fci-rule-color "#073642")
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
'(highlight-symbol-colors
(--map
 (solarized-color-blend it "#002b36" 0.25)
 (quote
  ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
'(highlight-tail-colors
(quote
 (("#073642" . 0)
  ("#546E00" . 20)
  ("#00736F" . 30)
  ("#00629D" . 50)
  ("#7B6000" . 60)
  ("#8B2C02" . 70)
  ("#93115C" . 85)
  ("#073642" . 100))))
'(hl-bg-colors
(quote
 ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
'(hl-fg-colors
(quote
 ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(inhibit-startup-screen t)
'(jabber-account-list
(quote
 (("miles.frankel@gmail.com"
   (:network-server . "talk.google.com")
   (:port . 5223)
   (:connection-type . ssl)))))
 '(magit-diff-use-overlays nil)
 '(mail-host-address "miles.frankel@gmail.com")
'(org-latex-default-packages-alist
(quote
 (("AUTO" "inputenc" t)
  ("T1" "fontenc" nil)
  ("" "fixltx2e" nil)
  ("" "graphicx" t)
  ("" "longtable" nil)
  ("" "float" nil)
  ("" "wrapfig" nil)
  ("" "rotating" nil)
  ("normalem" "ulem" t)
  ("" "amsmath" t)
  ("" "textcomp" nil)
  ("" "marvosym" t)
  ("" "amssymb" t)
  ("" "hyperref" nil)
  "\\tolerance=1000")))
 '(pdf-latex-command "/usr/local/texlive/2014/bin/x86_64-linux/pdflatex")
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(send-mail-function (quote smtpmail-send-it))
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(tool-bar-mode nil)
 '(user-full-name "Miles Frankel")
 '(vc-annotate-background nil)
'(vc-annotate-color-map
(quote
 ((20 . "#dc322f")
  (40 . "#c85d17")
  (60 . "#be730b")
  (80 . "#b58900")
  (100 . "#a58e00")
  (120 . "#9d9100")
  (140 . "#959300")
  (160 . "#8d9600")
  (180 . "#859900")
  (200 . "#669b32")
  (220 . "#579d4c")
  (240 . "#489e65")
  (260 . "#399f7e")
  (280 . "#2aa198")
  (300 . "#2898af")
  (320 . "#2793ba")
  (340 . "#268fc6")
  (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
'(weechat-color-list
(quote
 (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(iedit-occurrence ((t (:inherit nil :underline t)))))
