(require 'uniquify) ;; builtin
(require 'saveplace)

(setq ring-bell-function 'ignore
      backup-directory-alist '(("." . "~/.saves"))
      next-line-add-newlines t
      ido-enable-flex-matching t
      ido-everywhere t
      uniquify-buffer-name-style 'forward
      x-select-enable-clipboard t
      scroll-margin 5
      kill-buffer-query-functions (remq 'process-kill-buffer-query-function
                                        kill-buffer-query-functions))

(setq-default c-basic-offset 1
              show-trailing-whitespace t
              save-place t
              indent-tabs-mode nil
              x-select-enable-primary t
              save-interprogram-paste-before-kill t
              mouse-yank-at-point t
              require-final-newline t
              scroll-preserve-screen-position 1)

(delete-selection-mode 1)
(blink-cursor-mode 0)
(desktop-save-mode 1)
(global-linum-mode 1)
(column-number-mode)
(fset 'yes-or-no-p 'y-or-n-p)
(show-paren-mode t)
(recentf-mode t)
(add-hook 'prog-mode-hook 'subword-mode)
(electric-pair-mode 1)
(ido-mode 1)  ; when creating new file, disable with c-f

(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(add-hook 'window-setup-hook 'on-after-init) ;; remove background colour on X-less instances
(add-hook 'makefile-mode 'indent-tabs-mode)
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'before-save-hook (lambda() (delete-trailing-whitespace)))
