(defun kill-matching-lines (regexp &optional rstart rend interactive)
  "Kill lines containing matches for REGEXP.
See `flush-lines' or `keep-lines' for behavior of this command.
If the buffer is read-only, Emacs will beep and refrain from deleting
the line, but put the line in the kill ring anyway.  This means that
you can use this command to copy text from a read-only buffer.
\(If the variable `kill-read-only-ok' is non-nil, then this won't
even beep.)"
  (interactive
   (keep-lines-read-args "Kill lines containing match for regexp"))
  (let ((buffer-file-name nil)) ;; HACK for `clone-buffer'
    (with-current-buffer (clone-buffer nil nil)
      (let ((inhibit-read-only t))
        (keep-lines regexp rstart rend interactive)
        (kill-region (or rstart (line-beginning-position))
                     (or rend (point-max))))
      (kill-buffer)))
  (unless (and buffer-read-only kill-read-only-ok)
    ;; Delete lines or make the "Buffer is read-only" error.
    (flush-lines regexp rstart rend interactive)))

(defun exec-buffer-command ()   ;;COM: (progn (message "hi") (message "bye"))
  "executes one buffer command in the form of a comment: \
two comment characters followed bye 'COM:', e.g. ##COM: (...)\
in languages with multiline comments, i think that is the character referred to"
  (interactive)
  (push-mark-no-activate)
  (goto-char (point-min))
  (search-forward (concat comment-start comment-start "COM:"))
  (forward-sexp)
  (eval-last-sexp nil)
  (jump-to-mark))

(defun buffer-dir ()
  (interactive)
  (kill-new default-directory)
  (message "Path copied: 「%s」" default-directory))

(defun touchpad-on ()
  (interactive)
  (shell-command " xinput --enable  12" nil nil))

(defun touchpad-off ()
  (interactive)
  (shell-command " xinput --disable  12" nil nil))

(defun touchpad-toggle ()
  (interactive)
  (let
      ((out (shell-command-to-string "xinput list-props 12 |awk ' /Device Enabled/ {print $4}' | tr \"\\n\" \"0\" ")))
    (if (string= (eval out) "10")
        (touchpad-off)
      (touchpad-on))))

(defun save-macro (name)
  "save a macro. Take a name as argument
     and save the last defined macro under
     this name at the end of your .emacs"
  (interactive "SName of the macro :")  ; ask for the name of the macro
  (kmacro-name-last-macro name)         ; use this name for the macro
  (find-file user-init-file)            ; open ~/.emacs or other user init file
  (goto-char (point-max))               ; go to the end of the .emacs
  (newline)                             ; insert a newline
  (insert-kbd-macro name)               ; copy the macro
  (newline)                             ; insert a newline
  (switch-to-buffer nil))               ; return to the initial buffer

(defun on-after-init ()
  "Turn off background colour when not running in X"
  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))

(defun open-server ()
  (interactive)
  (find-file "/ssh:miles@68.175.70.96#4200:/home/miles/"))

(defun push-mark-no-activate ()
  "use mark without selecting"
  (interactive)
  (push-mark (point) t nil)
  (message "pushed mark to ring"))

(defun jump-to-mark ()
  "jump to mark"
  (interactive)
  (set-mark-command 1))

(defun ido-recentf-open ()
  (interactive)
  (if (find-file (ido-completing-read " Find recent file: " recentf-list))
      (message "Opening file...")
    (message "aborting")))

;; Variation of `zap-to-char'.

(defun zap-up-to-char (arg char)
  "Kill up to, but not including ARGth occurrence of CHAR.
Case is ignored if `case-fold-search' is non-nil in the current buffer.
Goes backward if ARG is negative; error if CHAR not found.
Ignores CHAR at point."
  (interactive "p\ncZap up to char: ")
  (let ((direction (if (>= arg 0) 1 -1)))
    (kill-region (point)
                 (progn
                   (forward-char direction)
                   (unwind-protect
                       (search-forward (char-to-string char) nil nil arg)
                     (backward-char direction))
                   (point)))))
