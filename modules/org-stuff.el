(setq org-directory (concat emacs-dir "/org"))
(setq org-default-notes-file (concat org-directory "/refile.org"))
(define-key global-map (kbd "C-c c") 'org-capture)
